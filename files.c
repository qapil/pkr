#include <dir.h>
#include <stdio.h>
#include <ctype.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include "pkr.h"
#include "files.h"

ulong VERSION = 0x01524B50;       // 01 (_) je cislo verze, zbytek je "PKR_"

ulong ar_size;
char ar_cont;

static int ret = 3*sizeof(long);  // o kolik se ma vratit pri zapisu delky

static char archiv[MAXPATH];
static int ar_count;

ulong write_header(FTYPE type, char *name, void *ar)
{
  ulong tmp;

  if (fwrite(&type, 1, 1, outfile) == 0) error(ER_WRITE);
  ar_size += 1;

  tmp=strlen(name);
  if (fwrite(&tmp, 1, 1, outfile) == 0) error(ER_WRITE);
  if (fwrite(name, tmp, 1, outfile) == 0) error(ER_WRITE);
  ar_size += tmp+1;

  switch (type)
  {
  case FT_FILE:
       tmp = sizeof(struct ar_file);
       break;
  case FT_DIREC:
       tmp = sizeof(struct ar_direc);
       break;
  }
  if (fwrite(ar, tmp, 1, outfile) == 0) error(ER_WRITE);
  ar_size += tmp;
  return ftell(outfile)-sizeof(ulong);
}
void write_size(ulong pos, ulong size)
{
  fseek(outfile, -size-ret, SEEK_CUR);

  if (fwrite(&pos, sizeof(pos), 1, outfile) == 0) error(ER_WRITE);
  if (fwrite(&size, sizeof(size), 1, outfile) == 0) error(ER_WRITE);

  fseek(outfile, 0, SEEK_END);
}
void write_pos(ulong pos)       // zapise aktualni pozici v outfile na pozici
{                               // pos v outfile
  ulong tmp = ftell(outfile);

  fseek(outfile, pos, SEEK_SET);
  if (fwrite(&tmp, sizeof(tmp), 1, outfile) == 0) error(ER_WRITE);
  fseek(outfile, tmp, SEEK_SET);
}
void write_cont()
{
  char tmp = 1;

  fseek(outfile, 4, SEEK_SET);
  if (fwrite(&tmp, 1, 1, outfile) == 0) error(ER_WRITE);
  fseek(outfile, 0, SEEK_END);
}
void padding()
{
  ulong tmp;

  fseek(outfile, 0, SEEK_END);
  tmp = ftell(outfile);
  while (tmp++ < vol_size) fputc(0, outfile);
}

void create_archiv(char *archiv)
{
  ulong tmp;

  if ((outfile=fopen(archiv, "rb")) == NULL) tmp = 1;
  else {                                                // soubor existuje
    if (fread(&tmp, 4, 1, outfile) == 0) error(ER_READ);

    if (fclose(outfile) == EOF) error(ER_CLOSE);
    if (overwr == 2) exit(0);                           // nesmim prepsat
    if (overwr == 0 && confirm == 0)                    // zeptam se
    {
      if (tmp == VERSION)
        printf("archiv %s uz existuje, ma se prepsat? [Ano/Ne/Vzdy] ", archiv);
      else
        printf("soubor %s uz existuje, ma se prepsat? [Ano/Ne/Vzdy] ", archiv);

      do tmp = tolower(getch());
      while (tmp != 'a' && tmp != 'n' && tmp != 'v');

      printf("\n");
      switch (tmp)
      {
      case 'v': overwr=1;
      case 'a': tmp = 1;
                break;
      case 'n': exit(0);
      }
    }
    else tmp = 1;
  }
  if (tmp == 1)
  {
    tmp = 0;
    if ((outfile = fopen(archiv,"wb")) == NULL) error(ER_OPEN);
    fwrite(&tmp, 0, 0, outfile);

    if (fwrite(&VERSION, 4, 1, outfile) == 0) error(ER_WRITE);
    if (fwrite(&tmp, 1, 1, outfile) == 0) error(ER_WRITE);      // cont = 0

    printf("\t\t\t\t\t\r"
           "vytvarim %s\n", archiv);
    ar_size = 5;
  }
}

void open_archiv(char *archiv)
{
  long tmp;

  if ((infile = fopen(archiv, "rb")) == NULL) error(ER_OPEN);
  if (fread(&tmp, 4, 1, infile) == 0) error(ER_READ);
  if (tmp != VERSION) error(ER_BADF);                  //testuj verzi archivu
  if (fread(&ar_cont, 1, 1, infile) == 0) error(ER_READ);
  printf("\t\t\t\t\t\t\r"
         "otviram %s\n", archiv);
}

void first_archiv(char *name, void (*f)(char *))
{
  char *p;
  int i;

  strcpy(archiv, name);

  if ((p = strrchr(archiv, '.')) == NULL)
  {
    ar_count = 0;
    strcat(archiv, ".pkr");
  }
  else {
    if (isdigit(p[2]) && isdigit(p[3]))
      ar_count = atoi(p+2)+1;
    else
      ar_count = 0;
    p[4] = '\0';
  }
  f(archiv);
}

void next_archiv(void (*f)(char *))
{
  char *p;
  int tmp;

  p = archiv+strlen(archiv)-2;
  if (ar_count > 99) error(ER_MUCHFILES);
  else if (ar_count > 9)
    itoa(ar_count, p, 10);
  else
  {
    p[0] = '0';
    itoa(ar_count, p+1, 10);
  }

  ar_count ++;

  if (confirm) tmp = 1;
  else {
    printf("mam pokracovat dalsim archivem (%s)? [Ano/Ne/Vzdy/nIkdy] ", archiv);

    do tmp = tolower(getch());
    while (tmp != 'a' && tmp != 'n' && tmp != 'v' && tmp != 'i');

    printf("\n");
    switch (tmp)
    {
    case 'v': confirm = 1;
    case 'a': tmp = 1;
              break;
    case 'i': confirm = 2;
    case 'n': tmp = 0;
              break;
    }
  }

  if (tmp) f(archiv);
  else exit(0);
}
