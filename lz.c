#include "pkr.h"
#include "files.h"

#define BUF_SIZE 4096
#define WIN_SIZE 18
#define MIN_LEN 3

//---------------------------------------------------------------------------

FILE *infile,
     *outfile;

uchar buffer[BUF_SIZE+WIN_SIZE-1],
      code_buf[17];             // 0 flags 1..16 code
int code_ptr,
    max_pos,
    max_len,
    l_syn[BUF_SIZE+1],
    p_syn[BUF_SIZE+257],        // >= BUF_SIZE+1 jsou koreny vyhledavacich stromu pro jednotlive znaky
    otec[BUF_SIZE+1];
// BUF_SIZE+1 je tu proto, ze index BUF_SIZE je pouzit jako identifikator listu


//---------------------------------------------------------------------------

void InitTree(void)
{
  int i;

  for (i=BUF_SIZE+1; i <= BUF_SIZE+256; i++)    // vynuluj (nastav BUF_SIZE) koreny stromu
    p_syn[i]=BUF_SIZE;
  for (i=0; i < BUF_SIZE; i++)
    otec[i]=BUF_SIZE;
}

void InsertNode(int r)
{
  int i,p,cmp;
  uchar *key;

  cmp=1;
  key=&buffer[r];                               // vkladany retezec
  p=BUF_SIZE+1+key[0];                          // koren stromu
  p_syn[r]=l_syn[r]=BUF_SIZE;                   // oznac list
  max_len=0;
  for ( ; ; )
  {
    if (cmp >= 0)
    {                                           // prava podvetev
      if (p_syn[p] != BUF_SIZE)                 // p neni list
        p=p_syn[p];
      else {
        p_syn[p]=r;                             // vloz retezec
        otec[r]=p;
        return;
      }
    }
    else {                                      // leva podvetev
      if (l_syn[p] != BUF_SIZE)                 // p neni list
        p=l_syn[p];
      else {
        l_syn[p]=r;                             // vloz retezec
        otec[r]=p;
        return;
      }
    }
//    for ( ; i < WIN_SIZE; i++)		// melo by fungovat i takhle, i=1 se musi udelat pred for cyklem
    for (i=1; i < WIN_SIZE; i++)
      if ((cmp=key[i]-buffer[p+i]) != 0) break; // porovnavej
    if (i > max_len)
    {                                           // je to nejdelsi vyskyt
      max_pos=p;
      if ((max_len=i) >= WIN_SIZE) break;       // je delsi nez prohledavaci okno
    }
  }
  otec[r]=otec[p];                              // misto p dej r
  l_syn[r]=l_syn[p];
  p_syn[r]=p_syn[p];
  otec[l_syn[r]]=r;
  otec[p_syn[r]]=r;
  if (p_syn[otec[p]] == p)
    p_syn[otec[p]]=r;
  else
    l_syn[otec[p]]=r;
  otec[p]=BUF_SIZE;                             // a vymaz p
}

void DeleteNode(int p)
{
  int q;

  if (otec[p] == BUF_SIZE) return;              // neni syn
  if (p_syn[p] == BUF_SIZE) q=l_syn[p];         // nema praveho syna
  else if (l_syn[p] == BUF_SIZE) q=p_syn[p];    // nema leveho syna
       else {                                   // ma oba syny
         q=l_syn[p];                            // q bude nejpravejsi prvek v levem podstromu
         if (p_syn[q] != BUF_SIZE)
         {
           q=p_syn[q];                          // jdi co nejvice doprava
           while (p_syn[q] != BUF_SIZE)
             q=p_syn[q];
           p_syn[otec[q]]=l_syn[q];             // zbav se leveho syna
           otec[l_syn[q]]=otec[q];
           l_syn[q]=l_syn[p];                   // pripoj noveho leveho syna
           otec[l_syn[p]]=q;
         }
         p_syn[q]=p_syn[p];                     // pripoj noveho praveho syna
         otec[p_syn[p]]=q;
       }
  otec[q]=otec[p];                              // q pujde na misto p
  if (p_syn[otec[p]] == p)
    p_syn[otec[p]]=q;
  else
    l_syn[otec[p]]=q;
  otec[p]=BUF_SIZE;                             // vymaz p
}

char encode(char *name, ulong file_size, ulong max_size, ulong *code_size)
{
  uchar mask;
  int i,c,
      s,w,
      len,
      last_max_len = 0;
  ulong print_size = 0,
        orig_size = 0;

  InitTree();                                   // inicializuj vyhledavaci strom
  *code_size = 0;
  code_buf[0] = 0;                              // vymaz flags
  code_ptr = mask = 1;
  s = 0;                                        // zacatek vyhledavani v bufferu
  w = BUF_SIZE-WIN_SIZE;                        // zacatek okna

  for (i=s; i<w; i++) buffer[i] = ' ';          // vymaz buffer

  for (len=0; len < WIN_SIZE && (c=getc(infile)) != EOF; len++)
    buffer[w+len] = c;                          // precti WIN_SIZE bytu do bufferu

  if ((orig_size=len) == 0) return 0;           // soubor delky 0 nekomprimuj

  for (i=WIN_SIZE; i >= 1; i--)
    InsertNode(w-i);                            // vloz retezce do vyhledavaciho stromu
  InsertNode(w);                                // nakonec posledni obsah bufferu

  max_len = 0;

  while (len > 0)
  {
    if (max_len > len) max_len = len;           // pouzije se na konci souboru
    if (max_len < MIN_LEN)
    {                                           // vystupem je znak
      max_len = 1;
      code_buf[0] |= mask;
      code_buf[code_ptr++] = buffer[w];
    }
    else {                                      // vystupem je (pozice,delka)
      code_buf[code_ptr++] = (uchar) max_pos;   // dolnich 8 bitu pozice
      code_buf[code_ptr++] = (uchar) ( ((max_pos>>4) & 0xf0)    // horni 4 bity pozice
                                       | (max_len-(MIN_LEN)) ); // a 4 bity delky
    }
    if ((mask<<=1) == 0)
    {                                           // uloz buffer na disk
      for (i=0; i < code_ptr; i++)
        if (putc(code_buf[i],outfile) == EOF) error(ER_WRITE);

      ar_size += code_ptr;
      *code_size += code_ptr;

      code_buf[0] = 0;
      code_ptr = mask = 1;
    }
    last_max_len = max_len;
    for (i=0; i < last_max_len && (c=getc(infile)) != EOF; i++)
    {
      DeleteNode(s);                            // vymaz uzel
      buffer[s] = c;
      if (s < WIN_SIZE-1) buffer[BUF_SIZE+s]=c; // na konci bufferu pro snazsi vyhledavani
      s = (s+1) & (BUF_SIZE-1);
      w = (w+1) & (BUF_SIZE-1);
      InsertNode(w);                            // vloz novy uzel a najdi maximalni vyskyt
    }
    if ((orig_size += i) > print_size)
    {                                           // vytiskni procento zpracovanych bytu
      printf(" %-30s \t%.1f\%\r", name, (double) 100*orig_size/file_size);
      print_size += 1024;
    }
    while (i++ < last_max_len)
    {                                           // pouzije se na konci souboru
      DeleteNode(s);
      s = (s+1) & (BUF_SIZE-1);
      w = (w+1) & (BUF_SIZE-1);
      if (--len) InsertNode(w);
    }
    if (len && ar_size > max_size-17)
    {
      fseek(infile, -len, SEEK_CUR);            // vrat se zpet o to co je v bufferu
      printf(" %-30s \tsplit  \n", name);
      return 1;                                 // bude pokracovat v dalsim archivu
    }
  }

  if (code_ptr > 1)                             // uloz zbytek bufferu
  {
    for (i=0; i < code_ptr; i++)
      if (putc(code_buf[i], outfile) == EOF) error(ER_WRITE);

    *code_size += code_ptr;
    ar_size += code_ptr;
  }

  return 0;
}

void decode(char *name, ulong size)
{
  int i,c,
      pos,len,w;
  uint flags;
  ulong print = size,
        orig  = size;

  if (size == 0) return;

  for (i = 0; i < BUF_SIZE-WIN_SIZE; i++) buffer[i] = ' ';   // vymaz buffer
  w = BUF_SIZE-WIN_SIZE;                        // zacatek okna
  flags = 0;
  for ( ; ; )
  {
    if (( (flags>>=1) & 0x0100) == 0)
    {                                           // precti znak
      if ((c=getc(infile)) == EOF) break;

      flags = c | 0xFF00;
    }
    if (flags & 0x01)
    {                                           // v bufferu je znak
      if ((c = getc(infile)) == EOF) break;

      putc(c, outfile);                         // znak na vystup
      buffer[w++] = c;                          // znak do bufferu
      w &= (BUF_SIZE-1);
      size --;

      if (print >= size)
      {
        print -= 1024;
        printf(" %-30s \t%.1f\%\r", name, (double) 100*(orig-size)/orig);
      }

      if (size == 0) return;                    // rozkodoval jsem tolik kolik jsem zakodoval
    }
    else {                                      // v bufferu je (pozice,delka)
      if ((pos=getc(infile)) == EOF || (len=getc(infile)) == EOF) break;
      pos |= ((len&0xF0)<<4);
      len = (len&0x0F)+MIN_LEN-1;
      for (i=0; i <= len; i++)
      {                                         // kopiruj buffer
        c = buffer[(pos+i)&(BUF_SIZE-1)];
        putc(c, outfile);
        buffer[w++] = c;
        w &= (BUF_SIZE-1);
        size --;

        if (print >= size)
        {
          print -= 1024;
          printf(" %-30s \t%.1f\%\r", name, (double) 100*(orig-size)/orig);
        }

        if (size == 0) return;                  // rozkodoval jsem tolik kolik jsem zakodoval
      }//for
    }// else
  }// for
  printf("pravdepodobne doslo pri kompresi nebo dekompresi k chybe\n");
}
