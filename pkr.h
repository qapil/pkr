#include <stdio.h>

// konstanty
#define ER_ARGS         1
#define ER_HELP         2
#define ER_OPEN         3
#define ER_CLOSE        4
#define ER_READ         5
#define ER_WRITE        6
#define ER_BADF         7
#define ER_WROP         8
#define ER_MUCHFILES    9
#define ER_LOMEM        10
#define ER_MKDIR        11
#define ER_RMDIR        12
#define ER_NOPATH       13

// typy
typedef unsigned char uchar;
typedef unsigned int uint;
typedef unsigned long int ulong;

// globalni promenne
extern char confirm;
extern char overwr;
extern ulong vol_size;
extern FILE *infile,
            *outfile;

// globalni funkce
extern void error(int e);
