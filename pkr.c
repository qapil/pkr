#include <stdlib.h>
#include <io.h>
#include <string.h>
#include <ctype.h>
#include <values.h>
#include <dos.h>
#include <dir.h>
#include <conio.h>
#include <process.h>
#include "pkr.h"
#include "lz.h"
#include "files.h"

#define FA_FALL FA_RDONLY | FA_HIDDEN | FA_SYSTEM | FA_ARCH
#define FA_DALL FA_FALL | FA_DIREC

struct flist
{
  FTYPE type;
  char *name;
  union {
    ulong start_pos;                    // FT_FILE
    struct flist *include;              // FT_DIREC
  };
  struct flist *up;
  struct flist *next;
};

struct info
{
  ulong count;
  ulong size;
  ulong packed;
};

// globalni promenne

struct flist *flist;
char rot[] = "-\\|/";
int rt = 0;

extern FILE *infile,
            *outfile;
char recurse = 0,
     nopaths = 0,
     confirm = 0,
     overwr  = 0;
ulong vol_size = 0;
char *archiv;                           // jmeno archivu

// funkce
void error(int e)
{
  switch (e)
  {
  case ER_ARGS:
       fprintf(stderr, "spatne zadane argumenty\n");
  case ER_HELP:
       fprintf(stderr, "\nPKR prikaz -[volby] archiv.pkr [soubor1 [soubor2 [... souborN]]]\n\n"
                       "prikazy: a   vytvor archiv\n"
                       "         e   rozbal archiv do aktualniho adresare\n"
                       "         x   rozbal archiv s plnymi cestami\n"
                       "         l   vypis obsah archivu\n"
                       "         v   vypis obsah archivu s cestami\n"
                       "         ?   tato napoveda\n"
                       "\n"
                       "volby:   r   projdi i adresare\n"
                       "         e   vynechej nazvy adresaru\n"
                       "         y   na vsechny dotazy odpovidej Ano (Yes)\n"
                       "         o+  vzdy prepisovat existujici soubory\n"
                       "         o-  nikdy neprepisovat existujici soubory\n"
                       "         vN  archiv velikosti N*1024 bytu\n"
              );
       break;
  case ER_OPEN:
       fprintf(stderr, "chyba pri otvirani souboru\n");
       break;
  case ER_CLOSE:
       fprintf(stderr, "chyba pri zavirani souboru\n");
       break;
  case ER_READ:
       fprintf(stderr, "chyba pri cteni souboru\n");
       break;
  case ER_WRITE:
       fprintf(stderr, "chyba pri zapisovani do souboru\n");
       break;
  case ER_BADF:
       fprintf(stderr, "chyba: spatny soubor\n");
       break;
  case ER_WROP:
       fprintf(stderr, "chyba: nelze otevrit soubor pro zapis\n");
       return;
  case ER_MUCHFILES:
       fprintf(stderr, "chyba: prilis mnoho archivu\n");
       break;
  case ER_LOMEM:
       fprintf(stderr, "chyba: nedostatek pameti\n");
       break;
  case ER_MKDIR:
       fprintf(stderr, "chyba: nelze vytvorit adresar\n");
       break;
  case ER_RMDIR:
       fprintf(stderr, "chyba: nelze smazat adresar\n");
       break;
  case ER_NOPATH:
       fprintf(stderr, "chyba: cesta nenalezena\n");
       break;
  default:
       fprintf(stderr, "chyba cislo %i\n",e);
       break;
  }
  exit(e);
}

struct flist * find_dir(struct flist *fl, char *name)
{
  fl = fl->include;
  while (fl != NULL)
  {
    if (fl->type == FT_DIREC && strcmpi(fl->name, name) == 0) break;
    fl = fl->next;
  }
  return fl;
}
int find_file(struct flist *fl, char *name)
{
  fl = fl->include;
  while (fl != NULL)
  {
    if (fl->type == FT_FILE && strcmpi(fl->name, name) == 0) return 1;
    fl = fl->next;
  }
  return 0;
}
struct flist * add_dir(char *dir1, char *dir2)
{
  char dir[MAXPATH],
       tmp[MAXPATH],
       *p;
  int l;
  struct flist *fl1, *fl2;

  if ((p = strchr(dir1, ':')) != NULL)
  {
    tmp[0] = p[-1];
    tmp[1] = ':';
    tmp[2] = '\\';
    getcurdir(toupper(p[-1])-'A'+1, tmp+3);
  }
  else {
    tmp[0] = '\\';
    getcurdir(0, tmp+1);
  }

  strcpy(dir, dir1);
  strcat(dir, dir2);
  if ((l = strlen(dir)) != 0) dir[--l] = '\0';  // odstran lomitko na konci

  if (dir[0] != '\0' && dir[l-1] != ':')
  {
    if (chdir(dir)) error(ER_NOPATH);
    chdir(tmp);         // nastav puvodni adresar
  }
  // adresar existuje

  fl1 = flist;

  while (dir2[0] != '\0')
  {
    p = strchr(dir2, '\\');
    p[0] = '\0';

    if ((fl2 = find_dir(fl1, dir2)) == NULL)
    {
      if ((fl2 = new struct flist) == NULL) error(ER_LOMEM);

      fl2->type = FT_DIREC;
      fl2->name = (char *) malloc(strlen(dir2)+1);
      strcpy(fl2->name, dir2);
      fl2->include = NULL;

      fl2->up = fl1;
      fl2->next = fl1->include;
      fl1->include = fl2;
    }
    fl1 = fl2;

    p[0] = '\\';
    dir2 = p+1;
  }
  return fl1;
}
void find(struct flist *fl, char *name, char *dir1, char *dir2)
{
  int done;
  ffblk f;
  char dir[MAXPATH];
  char tmp[MAXPATH];
  struct flist *fl2;

  strcpy(dir, dir1);
  strcat(dir, dir2);

  strcpy(tmp, dir);
  strcat(tmp, name);
  done = findfirst(tmp, &f, FA_FALL);           // hledej jen soubory
  while (!done)
  {
    if (! find_file(fl, f.ff_name))
    {
      if ((fl2 = new struct flist) == NULL) error(ER_LOMEM);

      fl2->type = FT_FILE;
      fl2->name = (char *) malloc(strlen(f.ff_name)+1);
      strcpy(fl2->name, f.ff_name);
      fl2->start_pos = 0;

      fl2->up = fl;
      fl2->next = fl->include;
      fl->include = fl2;

      printf(" %c\r", rot[rt]);
      rt = (rt+1)%4;
    }
    done = findnext(&f);
  }

  if (recurse)
  {
    done = findfirst(tmp, &f, FA_DALL);         // hledej adresar daneho jmena
    while (!done && (f.ff_attrib & FA_DIREC) == 0)
      done = findnext(&f);
    if (!done)
    {
      strcpy(name, "*.*");                      // nasel adresar
    }
    else {
      strcpy(tmp, dir);                         // nenasel adresar
      strcat(tmp, "*.*");                       // hledej ve vsech podadresarich
      done = findfirst(tmp, &f, FA_DALL);
    }

    while (!done)                               // vlez do adresare
    {
      if (f.ff_attrib & FA_DIREC && strcmp(f.ff_name, ".") && strcmp(f.ff_name, ".."))
      {

        if ((fl2 = find_dir(fl, f.ff_name)) == NULL)
        {
          if ((fl2 = new struct flist) == NULL) error(ER_LOMEM);

          fl2->type = FT_DIREC;
          fl2->name = (char *) malloc(strlen(f.ff_name)+1);
          strcpy(fl2->name, f.ff_name);
          fl2->include = NULL;

          fl2->up = fl;
          fl2->next = fl->include;
          fl->include = fl2;


          printf(" %c\r", rot[rt]);
        }

        strcpy(tmp, dir2);
        strcat(tmp, f.ff_name);
        strcat(tmp, "\\");
        find(fl2, name, dir1, tmp);
      }
      done = findnext(&f);
    }
  }
}

ulong add_files(struct flist *fl, char * dir, ulong next)
{
  char tmp[MAXPATH];
  struct flist *fl2;
  struct ffblk f;
  struct ar_file ar_file;
  struct ar_direc ar_direc;
  ulong wr_size;
  int q;

  fl = fl->include;
  while (fl != NULL)
  {
    strcpy(tmp, dir);
    strcat(tmp, fl->name);
    switch (fl->type)
    {
    case FT_FILE:
         findfirst(tmp, &f, FA_FALL);

         if ((infile=fopen(tmp, "rb")) == NULL) error(ER_OPEN);
         fseek(infile, fl->start_pos, SEEK_SET);

         ar_file.attrib    = f.ff_attrib;
         getftime(fileno(infile),&ar_file.time);
         ar_file.size      = f.ff_fsize;
         ar_file.start_pos = fl->start_pos;
         ar_file.end_pos   = 0;
         ar_file.packed    = 0; // velikost v archivu bude upresnena pozdeji
         ar_file.next      = 0; // pozice dalsiho zaznamu v adresari

         if (ar_size >= vol_size-1-strlen(fl->name)-sizeof(struct ar_file)-1 )
         {
           if (fclose(infile) == EOF) error(ER_CLOSE);
           return 0;    // KO
         }
         if (next != 1) write_pos(next);

         next = write_header(fl->type, fl->name, &ar_file);

         q = encode(tmp, ar_file.size, vol_size, &wr_size);

         fl->start_pos = ftell(infile);

         write_size(fl->start_pos, wr_size);
         if (fclose(infile) == EOF) error(ER_CLOSE);

         if (q != 0) return 0;          // KO

         printf(" %-30s \tOk    \n", tmp);

         break;
    case FT_DIREC:
         if (nopaths)
         {
           strcat(tmp, "\\");
           if ((next=add_files(fl, tmp, next)) == 0) return 0;       // KO
         }
         else {
           printf(" %s\n", tmp);

           findfirst(tmp, &f, FA_DALL);

           ar_direc.attrib  = f.ff_attrib;
           ar_direc.include = 0;        // pozice obsahu adresare
           ar_direc.next    = 0;        // pozice dalsiho zaznamu v adresari

           if (ar_size >= vol_size-1-strlen(fl->name)-sizeof(struct ar_direc)-1 )
             return 0;  // KO

           if (next != 1) write_pos(next);

           next = write_header(fl->type, fl->name, &ar_direc);

           strcat(tmp, "\\");
           if (add_files(fl, tmp, next-sizeof(ulong)) == 0) return 0; // KO
         }

         break;
    }
    fl2 = fl;
    fl->up->include = fl->next;
    fl = fl->next;

    free(fl2->name);
    delete fl2;
  }
  return next;  // OK
}

int compare(char *vzor, char *slovo)
{
  int ok = 1,
      v  = 0,
      s  = 0;

  if (vzor[0] == '*' && strlen(vzor) == 1) return 0;
  if (slovo[0] == '\0' && vzor[0] != '\0') return 1;
  while (vzor[v] && ok)
  {
    switch (vzor[v])
    {
      case '?': if (!isalnum(slovo[s]) &&
                   (slovo[s] != '_') &&
                   (slovo[s] != '-')) ok = 0;
                break;
      case '*': if (compare(vzor+v+1, slovo+s) != 0)    // nesouhlasi
                  if (slovo[s]) return compare(vzor+v, slovo+s+1);
                  else return 1;
                return 0;                               // souhlasi
      default:  if (toupper(slovo[s]) != toupper(vzor[v])) ok = 0;
                break;
    }
    v++;
    s++;
  }
  if (slovo[s]) ok = 0;         // neprosel jsem ho cele
  return !ok;
}

ulong extract(char cd, ulong next, char *s1, char *s2)
{
  char *p,
       str[MAXPATH],
       del;
  ulong count,
        tmp;
  FTYPE type;
  char name[MAXPATH];
  struct ar_file ar_file;
  struct ar_direc ar_direc;

  count = 0;

  while (next != 0)
  {
    fseek(infile, next, SEEK_SET);

    type = FT_HEAD;
    if (fread(&type, 1, 1, infile) == 0) error(ER_READ);

    tmp = 0;
    if (fread(&tmp, 1, 1, infile) == 0) error(ER_READ);
    if (fread(&name, tmp, 1, infile) == 0) error(ER_READ);
    name[tmp] = '\0';

    switch (type)
    {
    case FT_FILE:
         if (fread(&ar_file, sizeof(struct ar_file), 1, infile) == 0) error(ER_READ);
         next = ar_file.next;
         break;

    case FT_DIREC:
         if (fread(&ar_direc, sizeof(struct ar_direc), 1, infile) == 0) error(ER_READ);
         next = ar_direc.next;
         break;
    }

    if (strlen(s1) != 0)                // hledam adresar do cesty
    {
      p = strchr(s1, '\\');
      p[0] = '\0';

      if (type == FT_DIREC)
      {
        if (stricmp(s1, name) == 0)     // souhlasi
        {
          if (cd)
            if (chdir(name))
            {
              del = 1;
              if (mkdir(name)) error(ER_MKDIR);
              chdir(name);
            }
            else del = 0;

          tmp = extract(cd, ar_direc.include, p+1, s2);

          if (cd)
          {
            chdir("..");
            if (tmp != 0) _dos_setfileattr(name, ar_direc.attrib);
            else if (del && rmdir(name)) error(ER_RMDIR);
          }

          count += tmp;
        }
      }
      p[0] = '\\';
    }
    else {                              // hledam vse
      switch (type)
      {
      case FT_FILE:
           if (compare(s2, name) == 0)  // souhlasi
           {
             if ((outfile=fopen(name, "rb")) == NULL)
               if (ar_file.start_pos == 0) tmp = 1;
               else tmp = 2;                    // v arhivu je pokracovani, ale soubor neexistuje
             else {                             // soubor uz existuje
               fseek(outfile, 0, SEEK_END);
               tmp = ftell(outfile);
               if (fclose(outfile) == EOF) error(ER_CLOSE);

               if (ar_file.start_pos != 0)
                 if (ar_file.start_pos != tmp)
                 {
                   printf("soubor %s uz existuje s nespravnou delkou\n", name);
                   tmp = 2;                     // neprepisuj
                 }
                 else tmp = 3;                  // pripoj
               else
                 if (overwr == 2) tmp = 2;      // nikdy neprepisuj
                 else
                   if (overwr == 0 && confirm == 0)
                   {
                     printf("soubor %s jiz existuje, ma se prepsat? [Ano/Ne/Vzdy/nIkdy] ", name);

                     do tmp = tolower(getch());
                     while (tmp != 'a' && tmp != 'n' && tmp != 'v' && tmp != 'i');

                     printf("\n");
                     switch (tmp)
                     {
                     case 'v': overwr = 1;
                     case 'a': tmp = 1;
                               break;
                     case 'i': overwr = 2;
                     case 'n': tmp = 2;
                               break;
                     }
                   }
                   else tmp = 1;
             }

             switch (tmp)       // otevri soubor
             {
             case 1:
                  if ((outfile=fopen(name, "wb")) != NULL) break;
                  else error(ER_WROP);

             case 2:
                  printf("preskakuji %s\n", name);
                  outfile = NULL;
                  break;

             case 3:
                  if ((outfile=fopen(name, "r+b")) == NULL)
                  {
                    error(ER_WROP);
                    printf("preskakuji %s\n", name);
                    outfile = NULL;
                  }
                  else fseek(outfile, 0, SEEK_END);
                  break;
             }

             if (outfile != NULL)
             {                                  // dekomprese souboru
               decode(name, ar_file.end_pos-ar_file.start_pos);

               if (ar_file.end_pos == ar_file.size) printf("\n");
               else printf(" %-30s \tsplit  \n", name);


               if (fflush(outfile) == EOF) error(ER_WRITE);
               setftime(fileno(outfile), &ar_file.time);
               if (fclose(outfile) == EOF) error(ER_CLOSE);
               if (_dos_setfileattr(name, ar_file.attrib))
                 perror(sys_errlist[errno]);
             }

             count += 1;
           }
           break;

      case FT_DIREC:
           if (compare(s2, name) == 0)
           {
             strcpy(str, "*");        // souhlasi
             tmp = 1;
           }
           else strcpy(str, s2);                                // nesouhlasi

           if (strchr(str, '*') || strchr(str, '?'))
           {
             if (cd)
               if (chdir(name))
               {
                 del = 1;
                 if (mkdir(name)) error(ER_MKDIR);
                 chdir(name);
               }
               else del = 0;

             tmp += extract(cd, ar_direc.include, "", str);

             if (cd)
             {
               chdir("..");
               if (tmp != 0) _dos_setfileattr(name, ar_direc.attrib);
               else if (del && rmdir(name)) error(ER_RMDIR);
             }

             count += tmp;
           }
           break;
      }
    }
  }
  return count;
}

ulong list(char path, ulong next, char *s0, char *s1, char *s2, struct info *info)
{
  char *p,
       str[MAXPATH],
       attr[] = "    ",
       del;
  ulong count,
        tmp;
  FTYPE type;
  char name[MAXPATH];
  struct ar_file ar_file;
  struct ar_direc ar_direc;

  count = 0;

  while (next != 0)
  {
    fseek(infile, next, SEEK_SET);

    type = FT_HEAD;
    if (fread(&type, 1, 1, infile) == 0) error(ER_READ);

    tmp = 0;
    if (fread(&tmp, 1, 1, infile) == 0) error(ER_READ);
    if (fread(&name, tmp, 1, infile) == 0) error(ER_READ);
    name[tmp] = '\0';

    switch (type)
    {
    case FT_FILE:
         if (fread(&ar_file, sizeof(struct ar_file), 1, infile) == 0) error(ER_READ);
         next = ar_file.next;
         break;

    case FT_DIREC:
         if (fread(&ar_direc, sizeof(struct ar_direc), 1, infile) == 0) error(ER_READ);
         next = ar_direc.next;
         break;
    }

    if (strlen(s1) != 0)                // hledam adresar do cesty
    {
      p = strchr(s1, '\\');
      p[0] = '\0';

      if (type == FT_DIREC)
      {
        if (stricmp(s1, name) == 0)     // souhlasi
        {
          strcat(s0, name);
          strcat(s0, "\\");
          tmp = list(path, ar_direc.include, s0, p+1, s2, info);
          count += tmp;
        }
      }
      p[0] = '\\';
    }
    else {                              // hledam vse
      switch (type)
      {
      case FT_FILE:
           if (compare(s2, name) == 0)  // souhlasi
           {
             if (path) printf(" %s%s\n               ", s0, name);
             else      printf(" %-12s  ", name);

             if (ar_file.start_pos != 0 || ar_file.end_pos != ar_file.size) strcpy(str, "  split");
             else sprintf(str, "%6.1f\%", ar_file.size ? (double) 100*ar_file.packed/ar_file.size : 100);

             if (ar_file.attrib & FA_ARCH)   attr[0] = 'A';
             else                            attr[0] = '-';
             if (ar_file.attrib & FA_SYSTEM) attr[1] = 'S';
             else                            attr[1] = '-';
             if (ar_file.attrib & FA_HIDDEN) attr[2] = 'H';
             else                            attr[2] = '-';
             if (ar_file.attrib & FA_RDONLY) attr[3] = 'R';
             else                            attr[3] = '-';

             printf("%9lu  %9lu %s   %2.2u-%2.2u-%2.2u   %2.2u:%2.2u   %s\n",
                    ar_file.size, ar_file.packed, str,ar_file.time.ft_day,
                    ar_file.time.ft_month, ar_file.time.ft_year+80,
                    ar_file.time.ft_hour, ar_file.time.ft_min, attr);

             info->count += 1;
             info->size += ar_file.end_pos-ar_file.start_pos+1;
             info->packed += ar_file.packed;
             count += 1;
           }
           break;

      case FT_DIREC:
           if (compare(s2, name) == 0) strcpy(str, "*");        // souhlasi
           else strcpy(str, s2);                                // nesouhlasi

           if (strchr(str, '*') || strchr(str, '?'))
           {
             strcat(s0, name);
             strcat(s0, "\\");
             tmp = list(path, ar_direc.include, s0, "", str, info);
             count += tmp;
           }
           break;
      }
    }
  }
  return count;
}

void main(int argc, char **argv)
{
  int i,j;
  char **files;         // jmena souboru a adresaru v archivu

  ulong tmp;
  char str[MAXPATH],
       name[MAXPATH],
       d1[MAXPATH],
       d2[MAXPATH],
       *p;

  struct info info;
//  struct ar_file ar_file;


// info o programu

  printf("PKR archiver by martan (c) 1998\n");

// zpracovani parametru

  if (argc == 1 || strcmp(argv[1], "?") == 0) error(ER_HELP);
  if (argc < 3 || argv[1][1] || strpbrk(argv[1], "aexlv") == NULL) error(ER_ARGS);
  for (i=2; argv[i][0] == '-'; i ++)    // zpracuj volby
  {
    for (j=1; argv[i][j]; j ++)
    {
      switch (argv[i][j])
      {
      case 'r':
           recurse = 1;
           break;

      case 'e':
           nopaths = 1;
           break;

      case 'y':
           confirm = 1;
           break;

      case 'o':
           switch (argv[i][++j])
           {
           case '+':
                overwr = 1;
                break;
           case '-':
                overwr = 2;
                break;
           default:
                j --;
           }
           break;

      case 'v':
           vol_size = 0;
           while (isdigit(argv[i][++j]))
           {
             vol_size = vol_size*10+argv[i][j]-'0';
           }
           vol_size = vol_size*1024;
           j--;
           break;

      default:
           error(ER_ARGS);
           break;
      }
    }
  }
  if (vol_size == 0) vol_size = MAXLONG;
  if (argc >= i) archiv = argv[i++];            // jmeno archivu
  files = argv+i;                               // jmena souboru a adresaru v archivu

// prace s archivem

  switch (argv[1][0])
  {
  case 'a':                                     // pridej do archivu
       if ((flist = new struct flist) == NULL) error(ER_LOMEM);
       flist->type = FT_HEAD;
       flist->name    = NULL;
       flist->include = NULL;
       flist->up      = NULL;
       flist->next    = NULL;

       while (*files)
       {
         strcpy(str, *files++);
         strrev(str);
         p = strstr(str, "..");
         if (p != NULL)
         {
           if (p[-1] == '\\') p -= 1;
           strcpy(d1, p);
           strrev(d1);
           p[0] = '\0';
           strcpy(d2, str);
           strrev(d2);
         }
         else {
           strrev(str);
           p = strchr(str, ':');
           if (p != NULL)
           {
             if (p[1] == '\\') p += 1;
             strcpy(d2, p+1);
             p[1] = '\0';
             strcpy(d1, str);
           }
           else {
             d1[0] = '\0';
             strcpy(d2, str);
           }
         }

         p = strrchr(d2, '\\');
         if (p != NULL)
         {
           strcpy(name, p+1);
           p[1] = '\0';
         }
         else {
           strcpy(name, d2);
           d2[0] = '\0';
         }
         if (strlen(name) == 0) strcpy(name, "*.*");

         if ((i = strlen(d1)) != 0 && d1[i-1] != '\\') strcat(d1, "\\");

         printf("   ... hledam soubory\r");
         find(add_dir(d1, d2), name, d1, d2);

       }
       first_archiv(archiv, create_archiv);     // vytvor prvni archiv

       while (add_files(flist, d1, 1L) == 0)     // zabal soubory
       {
         write_cont();
         padding();
         if (fclose(outfile) == EOF) error(ER_CLOSE);
         next_archiv(create_archiv);
       }
       delete flist;

       if (fclose(outfile) == EOF) error(ER_CLOSE);

       break;

  case 'e':
  case 'x':
       if (argv[1][0] == 'e') i = 0;            // nevytvarej adresare
       else i = 1;                              // vytvarej adresare

       if (strlen(*files) == 0) strcpy(str, "*");
       else str[0] = 0;

       while (strlen(*files) || strlen(str))
       {
         if (strlen(str) == 0) strcpy(str, *files++);
         if (strcmp(str, "*.*") == 0) str[1] = 0;       // "*"

         p = strrchr(str, '\\');
         if (p == NULL)
         {
           strcpy(name, str);
           str[0] = 0;
         }
         else {
           strcpy(name, p+1);
           p[1] = 0;
         }

         first_archiv(archiv, open_archiv);
         extract(i, 5, str, name);
         while (ar_cont)
         {
           if (fclose(infile) == EOF) error(ER_CLOSE);
           next_archiv(open_archiv);
           extract(i, 5, str, name);
         }
         if (fclose(infile) == EOF) error(ER_CLOSE);

         str[0] = 0;
       }

       break;

  case 'l':
  case 'v':
       if (strlen(*files) == 0) strcpy(str, "*");
       else str[0] = '\0';

       while (strlen(*files) || strlen(str))
       {
         if (strlen(str) == 0) strcpy(str, *files++);
         if (strcmp(str, "*.*") == 0) str[1] = 0;       // "*"

         p = strrchr(str, '\\');
         if (p == NULL)
         {
           strcpy(name, str);
           str[0] = 0;
         }
         else {
           strcpy(name, p+1);
           p[1] = 0;
         }

         first_archiv(archiv, open_archiv);

         if (argv[1][0] == 'l') // nevypisuj adresare
         {
           i = 0;
           printf(" Jmeno          ");
         }
         else {                 // vypisuj adresare
           i = 1;
           printf(" Cesta\\Jmeno\n                ");
         }
         printf("Velikost   Zabaleno   Pomer   Datum      Cas     Attr\n"
                "----------------------------------------------------------------------\n");

         info.count = info.size = info.packed = 0;
         d1[0] = 0;
         list(i, 5, d1, str, name, &info);
         if (fclose(infile) == EOF) error(ER_CLOSE);

         str[0] = 0;
       }

       printf("----------------------------------------------------------------------\n");
       printf("%9lu      %9lu  %9lu %6.1f\%\n", info.count, info.size,info.packed,
              info.size ? (double) 100*info.packed/info.size : 100);

       break;
  }
}
