#include "pkr.h"
#include <io.h>

enum FTYPE { FT_HEAD = 0, FT_FILE = 1, FT_DIREC = 2 };

struct ar_file
{
  char attrib;                  // atributy
  struct ftime time;            // datum a cas
  ulong size;                   // velikost originalu
  ulong start_pos;              // pocatecni pozice originalu
  ulong end_pos;                // koncova pozice originalu
  ulong packed;                 // velikos v archivu
  ulong next;                   // pozice dalsiho souboru ve stejnem adresari
};
struct ar_direc
{
  char attrib;                  // atributy
  ulong include;                // pozice prvni polozky uvnitr adresare
  ulong next;                   // pozice dalsi polozky ve stejnem adresari
};

extern ulong ar_size;
extern char ar_cont;

extern ulong write_header(FTYPE type, char *name, void *ar);
extern void write_size(ulong pos, ulong size);
extern void write_pos(ulong pos);
extern void write_cont();
extern void padding();

extern void create_archiv(char *name);
extern void open_archiv(char *name);

extern void first_archiv(char *name, void (*f)(char *));
extern void next_archiv(void (*f)(char *));
